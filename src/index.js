import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Demo from './Demo';

import * as serviceWorker from './serviceWorker';
import { FaRegStar } from "react-icons/fa";

ReactDOM.render(
  <React.StrictMode>
      <div>
    <Demo name="Oh my Deer" text="lorem ipsum dolor sit amet ddjdj s" price="15$" cardIcon={ <FaRegStar/> }/>
      <Demo  name="Awaken  " text="lorem ipsum dolor sit dmdm triir a" price="18$" cardIcon={ <FaRegStar/> } />
      <Demo   name="Ravenna" text="lorem ipsum dolor gg tt uu gggdd " price="25$"  cardIcon={ <FaRegStar /> }/>
      <Demo name="Awaken" text="lorem ipsum dolor aa tt uu rr " price="5$" cardIcon={ <FaRegStar/> }/>

      </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
